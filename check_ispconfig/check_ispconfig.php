#!/usr/bin/env php
<?php

/**
 * Nagios compatible script to export data from the monitor page.
 *
 * Outputs a single line like: `WARNING: (ok: 12, info: system_update, warning: sys_log)`
 *
 * Usage:
 *
 * In an NRPE compatibe config file, such as /etc/nagios/nrpe.cfg:
 * `command[check_ispconfig]=/usr/bin/sudo /usr/local/ispconfig/server/scripts/check_ispconfig.php`
 *
 * To allow an nrpe user to run this, /etc/sudoers.d/ispconfig:
 * ```
 * Cmnd_Alias  CHECK_ISPCONFIG = /usr/local/ispconfig/server/scripts/check_ispconfig.php
 *
 * nagios  ALL = NOPASSWD : CHECK_ISPCONFIG
 * ```
 */

/*
Copyright (c) 2021-03-07, Herman van Rink / Initfour websolutions
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

define('SCRIPT_PATH', dirname($_SERVER["SCRIPT_FILENAME"]));
require SCRIPT_PATH."/../lib/config.inc.php";
require SCRIPT_PATH."/../lib/app.inc.php";

ini_set('error_reporting', E_ALL & ~E_NOTICE);

$server_state = 'ok';
$retval = 0;
$server_states = array();
$state_list = array();

$records = $app->dbmaster->queryAllRecords("SELECT DISTINCT type, state FROM monitor_data
		WHERE server_id = ? AND state != 'no_state'", $conf['server_id']);

foreach($records as $record) {
	$server_states[$record['state']][] = $record['type'];
}

foreach ($server_states as $state => $data) {
	if ($state == 'ok') {
		$state_list[] = "$state: " . count($data);
	}
	else {
		$state_list[] = "$state: " . join(', ', $data);
	}
}
if (empty($server_states)) {
	$server_state = 'unknown';
	$retval = 3;
}
elseif (is_array($server_states['critical']) && count($server_states['critical']) > 0 || is_array($server_states['error']) && count($server_states['error']) > 0) {
	$server_state = 'critical';
	$retval = 2;
}
elseif ($server_states['warning'] > 0) {
	$server_state = 'warning';
	$retval = 1;
}

$status_line = strtoupper($server_state) . ': (' . join(', ', $state_list) . ')';

echo $status_line . PHP_EOL;
exit($retval);
